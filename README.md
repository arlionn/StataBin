&emsp; 

> 连享会 · 公开课：<br>Stata 学习经验分享：从入门到进阶   

> &#x1F449;  &#x1F34E;  点击右上角的【**Fork**】按钮，可以把这个项目完整复制到你的码云账号下，随时查看。 

&emsp;



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/龙志能公开课V6.png)

&emsp;

---

**目录**

[[TOC]]

---

&emsp;

## 1. 课程概览

- **听课方式：** 网络直播。支持手机、iPad、电脑等。
- **直播嘉宾**：龙志能 (上海财经大学博士生)
- **费用/软件**：免费，Stata
- **时间**：2020 年 11 月 10 日，19:00-20:00   
- **课程主页：** <https://gitee.com/arlionn/StataBin> 
  - 回放：本课程提供长期回放，可前往主页查看回放信息。
  - 课程资料：课程中涉及的资料和链接会通过课程主页发布。 
- **课程咨询：** 李老师-18636102467（微信同号）
- **学习方式：** 扫描海报中的二维码，加入课程群，随后统一发布听课链接。

&emsp; 

## 2. 课程导言

面对海量 Stata 资料，你是否时常不知从何学起？我也是，初识 Stata 的那个夏天，一时间毫无头绪，几月有余，学会了些许命令，直到看过同门师兄 Stata 操作，恍然大悟。我的学习方式恍如无头苍蝇，四处乱窜，毫无成效。

厉害的师兄就在身边，向他学习事半功倍。在师兄的指导和有计划学习下，我总算成功入门。我看过很多资料，上过不少课程；在连享会做助教，在实践中不断学习，对于 Stata 使用有一定进步。本次分享希望将我的经验和教训分享给需要的朋友。

本次课程旨在帮助大家搭建 Stata 入门和进阶的基本框架，推荐精选权威的各种资源，以及分享高效解决学习中疑难的基本方法。主要内容包括：

### Stata 入门
- **少走点弯路：初学者的学习路线**。分别从计量基础和 Stata 操作层面总结入门 Stata 所需要学习和掌握的关键知识点。
- **武林秘籍：学习资源分享**。对应 Stata 入门需学习的重点，精准推荐系统学习的书籍手册、推文链接和学习视频。

### Stata 进阶
- **步步为营：进阶者的学习路线探讨**。我们该学习哪些主要的计量方法和模型？Stata 操作层面要如何提升自己？要学习编程吗？
- **进阶者的学习资源推荐**。对应 Stata 进阶需掌握的重点，精选推荐便于查阅的书籍手册、推文链接和视频课程。

### 扫除取经路上的妖魔鬼怪
- **如何解决学习中遇到的难题**。学习中总会遇到各种困难，我们该如何快速找到解决方法？去哪里求助？如何求助？

### 说在最后
- 收藏 != 学会 (贪心+自我安慰)
- 看过 != 学会 (囫囵吞枣+眼高手低)
- 实际操作 + 分享讲解 + 写篇论文 = 真会了


&emsp;

## 3. 课程提要

- **Stata 入门**：学习路线总结
  - 基础不牢，地动山摇；基础 = 知识架构 + 核心概念
  - 初级计量基础：多元回归；模型筛选；虚拟变量和交乘项
  - Stata 基础操作
    - 基本数据处理（导入、合并、清洗、缺失值、异常值、重复值）
    - 描述性统计与OLS回归
    - 固定效应模型回归
  - Stata 入门的学习资源分享
    - 书籍、手册、推文、视频
- **Stata 进阶**：「干中学」最有效！
  - 中高级计量基础【适用条件+正确理解】
    - 内生性问题理解
    - 匹配方法（matching）
    - 工具变量法（IV）
    - 双重差分方法（DID）
    - 断点回归（RD）
  - Stata 进阶学习
    - 高级数据处理（处理复杂数据，Python和Stata融合）
    - 构建复杂变量（盈余管理，股价崩盘，过度投资）
    - 正确使用命令+适用条件检验+安慰剂检验
  - Stata 进阶的学习资源推荐
- **Q：** 如何解决学习中遇到的难题？
  - 搜索引擎、微信搜索
  - 垂直网站
  - 常见问题解答（FAQs）
  - 微信讨论群
  - 如何提问？如何发邮件？



&emsp;

## 4. 课程特色

- 短小精悍：快速了解 Stata 入门和进阶的基本框架和解决难题的各种方案
- 讲义资料：分享电子版课件，分享的框架和资源可用于指导自身学习。
- 课后答疑：课程结束后，分享嘉宾将提供问题解答。



&emsp;

## 5. 嘉宾简介

**龙志能**，上海财经大学博士生，主要研究方向为公司财务与公司治理，在《外国经济与管理》、《Research in International Business and Finance》发表论文。现为连享会直播课助教，曾参与 Stata 暑期班助教工作。参与并负责 2020 年 Stata 暑期班助教工作受到老师和学员的一致好评。

<img style="width: 150px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20201103-龙志能小图.jpg">



&emsp;

## 6. 相关资料

- **预习资料：** [连享会视频公开课 - Stata33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0)：
- **连享会推文：** [Stata入门](https://www.lianxh.cn/blogs/16.html)；[Stata教程](https://www.lianxh.cn/blogs/17.html)；[Stata资源](https://www.lianxh.cn/blogs/35.html)
- **进阶内容：** [Stata 寒假班](https://gitee.com/lianxh/PX) 对暂元、循环语句、数据合并等会有进一步的讲解 (即将上线)。

&emsp;


&emsp;


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) || [Bilibili 站](https://space.bilibili.com/546535876) 

---

## 1. 计量专题

&emsp;

> [最新上线](https://gitee.com/arlionn/collections/278581) | [公开课](https://gitee.com/arlionn/collections/278579) | [小专题](https://gitee.com/arlionn/collections/1257)

&emsp;

> 推文集合：[PDF 合集](https://file.lianxh.cn/KC/lianxh_Blogs.pdf)

- Stata：[教程](https://www.lianxh.cn/blogs/17.html) |  [资料](https://www.lianxh.cn/blogs/35.html) | [新命令](https://www.lianxh.cn/blogs/43.html) | [结果输出](https://www.lianxh.cn/blogs/22.html) | [绘图](https://www.lianxh.cn/blogs/24.html) | [数据处理](https://www.lianxh.cn/blogs/25.html) |  [程序](https://www.lianxh.cn/blogs/26.html)
- [回归分析](https://www.lianxh.cn/blogs/32.html) |  [面板数据](https://www.lianxh.cn/blogs/20.html) | [交乘项-调节](https://www.lianxh.cn/blogs/21.html)  | [IV-GMM](https://www.lianxh.cn/blogs/38.html) | [Logit](https://www.lianxh.cn/blogs/27.html) | [空间计量](https://www.lianxh.cn/blogs/29.html) 
- [因果推断](https://www.lianxh.cn/blogs/19.html) |  [DID](https://www.lianxh.cn/blogs/39.html) |  [RDD](https://www.lianxh.cn/blogs/40.html) |  [PSM](https://www.lianxh.cn/blogs/41.html) |  [合成控制](https://www.lianxh.cn/blogs/42.html) | [文本分析](https://www.lianxh.cn/blogs/36.html) | [机器学习](https://www.lianxh.cn/blogs/47.html)
- [Markdown](https://www.lianxh.cn/blogs/30.html)  | [工具软件](https://www.lianxh.cn/blogs/23.html) |  [其它](https://www.lianxh.cn/blogs/33.html)

> 热门推文
- [稳健性检验！稳健性检验！](https://www.lianxh.cn/news/32ae13ec789a1.html)
- [安慰剂检验！安慰剂检验！](https://www.lianxh.cn/news/cf8bd8363200d.html)
- [Stata: 面板数据模型一文读懂](https://www.lianxh.cn/news/bf27906144b4e.html)
- [Stata：聚类调整后的标准误-Cluster-SE](https://www.lianxh.cn/news/a7a8e613b2699.html)
- [Stata：DID入门教程](https://www.lianxh.cn/news/3849f237b6d36.html)
- [DID偏误问题：两时期DID的双重稳健估计量(上)-drdid](https://www.lianxh.cn/news/e6ef033e13c3e.html)
- [DID偏误问题：多时期DID的双重稳健估计量(下)-csdid](https://www.lianxh.cn/news/762e878e7063b.html)
- [Stata+R：一文读懂精确断点回归-RDD](https://www.lianxh.cn/news/96fb6b7e847e1.html)
- [内生性！内生性！解决方法大集合](https://www.lianxh.cn/news/224e2b4e170e4.html)
- [连享会：论文重现网站大全](https://www.lianxh.cn/news/e87e5976686d5.html)

&emsp;


> ### ${\color{blue}{\blacktriangleright}}$ [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

&emsp;

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-001.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-002.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-003.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-004.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-005.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-006.png)](https://www.lianxh.cn/news/46917f1076104.html)

> ### ${\color{red}{\blacktriangleright}}$ [计量专题](https://gitee.com/arlionn/Course)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 2. 公开课/专题课

### 公开课 
- 侯丹丹，Stata软件及计量基础，6 次课 x 2h，[去听课](https://lianxh.duanshu.com/#/brief/course/49c3dbf11982456db9be1b5f595285dc)，[课件](https://gitee.com/arlionn/stataopen)
- 连玉君，直击面板数据模型，1.4h，[去听课](http://lianxh-pc.duanshu.com/course/detail/7d1d3266e07d424dbeb3926170835b38)，[B站视频](https://www.bilibili.com/video/BV1oU4y187qY)，[课件](https://gitee.com/arlionn/PanelData)
- 连玉君，Stata 33 讲，15min/讲. [去听课](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0)，[B站视频](https://space.bilibili.com/546535876/channel/detail?cid=160748)，[课件](https://gitee.com/arlionn/stata101)  
- 连玉君，论文复现：why？what？How？，2h，[去听课](https://www.lianxh.cn/news/3544e941a02d8.html)，[B站视频](https://www.bilibili.com/video/BV1D54y1n7Fa)，[课件](https://gitee.com/arlionn/rep)
- 龙志能，Stata小白的取经之路，1.5h，[去听课](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530)，[课件](https://gitee.com/arlionn/StataBin)
- 涂冰倩，微观数据库清理经验分享，2h，[去听课](https://gitee.com/arlionn/dataclean/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE%EF%BC%9A%E6%B6%82%E5%86%B0%E5%80%A9-%E5%BE%AE%E8%A7%82%E6%95%B0%E6%8D%AE%E5%BA%93%E6%B8%85%E7%90%86%E7%BB%8F%E9%AA%8C%E5%88%86%E4%BA%AB.md#https://gitee.com/link?target=https%3A%2F%2Fwww.lianxh.cn%2Fblogs%2F16.html)，[课件](https://gitee.com/arlionn/dataclean)
- 游万海，实证分析中的数据可视化，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26)，[课件](https://gitee.com/arlionn/Rplot)
- 杨海生，如何玩转空间计量？，1.5h，[去听课](https://lianxh.duanshu.com/#/brief/course/675437c0c52f4e48947531286cef1e87)，[课件](https://gitee.com/arlionn/SP/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE-%E6%9D%A8%E6%B5%B7%E7%94%9F-%E7%8E%A9%E8%BD%AC%E7%A9%BA%E9%97%B4%E8%AE%A1%E9%87%8F.md)
- 范巧，空间计量经济学的基本框架及分析范式，[去听课](https://lianxh.duanshu.com/#/brief/course/6b294c08454042ce860e43363bbeed5b)，[课件](https://gitee.com/arlionn/SP/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE-%E8%8C%83%E5%B7%A7-%E7%A9%BA%E9%97%B4%E8%AE%A1%E9%87%8F%E7%BB%8F%E6%B5%8E%E5%AD%A6%E5%9F%BA%E6%9C%AC%E6%A1%86%E6%9E%B6%7C%E5%88%86%E6%9E%90%E8%8C%83%E5%BC%8F.md)
- 范巧，空间计量建模与高质量论文撰写，[去听课](https://lianxh.duanshu.com/#/brief/course/35252658d4a046a0b0b74ceea1f8e0ee)，[课件](https://gitee.com/arlionn/SP/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE-%E8%8C%83%E5%B7%A7-%E7%A9%BA%E9%97%B4%E8%AE%A1%E9%87%8F%E7%BB%8F%E6%B5%8E%E5%AD%A6%E5%9F%BA%E6%9C%AC%E6%A1%86%E6%9E%B6%7C%E5%88%86%E6%9E%90%E8%8C%83%E5%BC%8F.md)
- 龙志能，如何高效运用搜索解决问题？，1h，[去听课](https://lianxh.duanshu.com/#/brief/course/ac14b768d2314d43a8a805205a45d3e3)
- 王昆仑，双重差分方法的新进展-交错型DID，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/9f83a4513f7f409b96e40a1bd0c79379)
- [连享会码云：100多个精选计量项目](https://www.lianxh.cn/news/944a69d75cec9.html) |  [新浪视频](https://weibo.com/tv/show/1034:4479228373303338)
- [五分钟 Markdown / Markdown 幻灯片](https://gitee.com/arlionn/md) | [新浪视频](https://weibo.com/tv/show/1034:4484204327796746)
- [连老师给你的-听课建议](https://www.lianxh.cn/news/69706e871c9ad.html)

### 专题小课
- 连玉君，我的特斯拉-实证研究设计，2.2h，[去听课](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[课件](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)
- 连玉君，我的甲壳虫-论文精讲与重现，6h，[去听课](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)，[课件](https://gitee.com/arlionn/paper101)
- 连玉君，动态面板数据模型，2.2h，[去听课](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[课件](https://gitee.com/arlionn/Live/tree/master/%E8%BF%9E%E7%8E%89%E5%90%9B-%E5%8A%A8%E6%80%81%E9%9D%A2%E6%9D%BF%E6%A8%A1%E5%9E%8B)
- 游万海，Stata 数据清洗之实战操作 (第一季)，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/c5193f0e6e414a7e889a8ff9aeb4aaef)，[主页](https://gitee.com/arlionn/dataclean)
- 游万海，Stata 数据清洗之实战操作 (第二季)，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/23924488072b4e458ec3bb0a830b187f)，[主页](https://gitee.com/arlionn/dataclean)
- 游万海，分位数回归，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/f0bfb3102ada48969966c92123a7ebf0)

&emsp;

## 3. 资源分享

连享会主要通过三个渠道分享实证分析经验。

> **主页**：[www.lianxh.cn](https://www.lianxh.cn)  
  - 目前已分享 1000 余篇推文，包括 Stata/R/Python 等软件使用经验，[各类](https://www.lianxh.cn/blogs.html) 计量模型，内生性、DID、RDD 专题等。
  - **人手一份：Stata 101**，[PDF下载](https://file.lianxh.cn/KC/Slides/lianxh_Stata101.pdf), [HTML下载](https://file.lianxh.cn/KC/Slides/lianxh_Stata101.html) | [Stata Journal](https://www.lianxh.cn/news/12ffe67d8d8fb.html)
> **b 站**：<https://space.bilibili.com/546535876> 
  - 亦可在 b 站搜索关键词 **连享会**
  - 内容：浏览量超过 10 w 的公开课视频，包括 [Stata 33 讲](https://space.bilibili.com/546535876/channel/detail?cid=160748)、[直击面板数据模型](https://www.bilibili.com/video/BV1oU4y187qY)、[论文复现：why？what？How？](https://www.bilibili.com/video/BV1D54y1n7Fa) 等。
> **码云仓库**：<https://gitee.com/arlionn>
  - 连享会收集整理的代码仓库，分为 30 多个 [专题](https://gitee.com/arlionn/collections)，500+ 个 [仓库](https://gitee.com/arlionn/projects)，涉及论文复现代码、RDD，DID，Panel，SCM，Lasso 等最新方法实现代码等。
  - 用法：申请一个码云账号，然后点击仓库右上角的 **[Fork](https://gitee.com/arlionn/list#)** 按钮，将喜欢的仓库克隆到你的账号下，随后点击仓库名称右边的 $\curvearrowright$ 按钮强制同步。

### Data
- [CSMAR-国泰安](http://www.gtarsc.com/#/datacenter/singletable) | [Wind-万德](https://www.wind.com.cn/Default.html) | [Resset-锐思](http://www.resset.cn/databases)
- [常用数据库](https://www.lianxh.cn/news/0b65fd5165c2c.html) | [人文社科开放数据库](https://www.lianxh.cn/news/6f06c914acde8.html) | [知乎-Data](https://www.zhihu.com/question/20179699/answer/681756635)
- [徐现祥教授-IRE-官员交流、方言等](https://www.lianxh.cn/news/8c9f81a5f19ee.html)

### Papers - 学术论文复现
- [论文重现网站大全](https://www.lianxh.cn/news/e87e5976686d5.html)
- Stata论文重现：[Harvard][harvd] | [Top 期刊论文](https://ejd.econ.mathematik.uni-ulm.de/) | [JFE][jfe]  | [github][git1] 
- 学者主页：[Angrist][Ang1] | [Acemoglu][acem]  | [Levine][ross] | [Duflo][Duflo] | [Imbens](https://scholar.harvard.edu/imbens/software) | [Chetty](http://www.rajchetty.com/)

[harvd]:https://dataverse.harvard.edu/dataverse
[jfe]:http://jfe.rochester.edu/data.htm
[Ang1]:http://economics.mit.edu/faculty/angrist/data1/data
[acem]:http://economics.mit.edu/faculty/acemoglu/data
[ross]:http://faculty.haas.berkeley.edu/ross_levine/papers.htm
[duflo]:http://economics.mit.edu/faculty/eduflo/papers
[git1]:https://github.com/search?utf8=%E2%9C%93&q=stata&type=

[yahoogit]:https://search.yahoo.com/search;_ylt=AwrBT8di2LBZqyEAuG9XNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10LTQ3MwRncHJpZAMEbl9yc2x0AzAEbl9zdWdnAzAEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzE0BHF1ZXJ5A3N0YXRhJTIwZ2l0aHViBHRfc3RtcAMxNTA0NzYxODcz?p=stata+github&fr2=sb-top&fr=yfp-t-473&fp=1


&emsp;


## 4. 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，500+ 推文，实证分析不再抓狂；[Bilibili 站](https://space.bilibili.com/546535876) 有视频大餐。

><font color=red>New！</font> **`lianxh` 命令发布了！**    

- 随时搜索连享会推文、Stata 资源，安装命令如下：  
  &emsp; `. ssc install lianxh`  
- 使用详情参见帮助文件 (有惊喜)：   
  &emsp; `. help lianxh`

> 连享会小程序：扫一扫，看推文，看视频……

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)](https://www.lianxh.cn/news/46917f1076104.html)

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)
